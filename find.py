import cv2
import numpy as np
import os
import random

colors_dict = {}

colors_dict['blue'] = (255,55,55)
colors_dict['green'] = (55,255,55)
colors_dict['red'] = (55 ,55,255)
colors_dict['lab'] = (255,255,255)
colors_dict['yellow'] = (102, 255, 255)
colors_dict['purple'] = (255, 102, 255)

def randColor():
    return (random.randint(66,256), random.randint(66,256), random.randint(66,256))

img_rgb = cv2.imread('map.png')

for root, dirs, files in os.walk("templates"):
    print (files)
    for mfile in files:
        template_path = os.path.join(root, mfile)
        template = cv2.imread(template_path)
        w, h, d = template.shape

        res = cv2.matchTemplate(img_rgb, template, cv2.TM_CCOEFF_NORMED)


        threshold = 0.8

        
        mcolor = (0, 0, 0)

        loc = np.where(res >= threshold)
        for key in colors_dict:
            if (key in mfile):
                mcolor = colors_dict[key]
        if (mcolor == (0, 0, 0)):
            mcolor = randColor()
        
        for pt in zip(*loc[::-1]):
                cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), mcolor, 1)
                # cv2.putText(img_rgb, mfile, (pt[0] + 1, pt[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,255))
               
cv2.imshow('Detected', img_rgb)
cv2.waitKey() 
cv2.destroyAllWindows